float angle = 0;
float speed = 1;

color colorFrom;
color colorTo;

void setup(){
  size(400,400);
  colorMode(HSB,360,100,100);
  colorFrom = color(0,75,85);
  colorTo = color(288,75,85);
}

void draw(){
  background(200);
  translate(200,80);
  
  //stem
  strokeWeight(8);
  stroke(0,0,100);
  line(0,0,0,200);
  //petals
  pushMatrix();
    stroke(0);
    strokeWeight(2);
    noStroke();
    rotate(radians(angle));
    pushMatrix();
      for(int i = 0; i < 20; i++){
        fill(lerpColor(colorFrom,colorTo,map(i,0,19,0,1)));
        ellipse(0,30,20,60);
        rotate(radians(30));
      }
    popMatrix();
  popMatrix();
  angle += speed;
}

void mousePressed(){
  colorFrom = color(random(360),random(50,100),random(50,100),255);
  colorTo = color(random(360),random(50,100),random(50,100),255);
}