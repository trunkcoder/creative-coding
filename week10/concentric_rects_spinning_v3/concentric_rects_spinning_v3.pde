
float angle = 0;
float initialSpeed = 0.01;
float speed = initialSpeed;
int direction = 1;

int startSize = 130;
int steps = 9;

color color1;
color color2;

void setup(){
  size(200,200,P2D);
  rectMode(CENTER);
  colorMode(HSB,360,100,100);
  color1 = color(#D344CC);
  color2 = (#46D344);
  noStroke();
}

void draw(){
  background(200);
  translate(100,100);
  fill(0,100,100,100);
  for(int i = startSize; i > steps; i -= steps){
    pushMatrix();
      rotate(radians(angle * direction));
      fill(lerpColor(color1,color2,map(i,steps,startSize,0,1)));
      pushMatrix();
      int direction1;
      int direction2;
        if(random(1) > 0.5){
          direction1 = 1;
        } else {
          direction1 = -1;
        }
        if(random(1) > 0.5){
          direction2 = 1;
        } else {
          direction2 = -1;
        }
        translate(random(steps * direction1),random(steps * direction2));
        rect(0,0,i,i);
      popMatrix();
      direction = direction * -1;
      speed = speed * 1.2;
      angle += speed;
    popMatrix();
  }
  speed = initialSpeed;
}

void mousePressed(){
  color1 = color(random(360),random(50,100),random(50,100),150);
  color2 = color(random(360),random(50,100),random(50,100),150);
}