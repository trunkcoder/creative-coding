
float angle = 0;
float initialSpeed = 0.01;
float speed = initialSpeed;
int direction = 1;

int startSize = 300;//startSize = 180
int steps = 20;

color colorFrom;
color colorTo;

void setup(){
  size(200,200,P2D);
  rectMode(CENTER);
  colorMode(HSB,360,100,100);
  colorFrom = color(#D344CC);
  colorTo = color(#46D344);
  noFill();
  strokeWeight(10);
}

void draw(){
  background(0,100,0);
  translate(100,100);
  //fill(0,100,100,100);
  for(int i = startSize; i > steps; i -= steps){
    pushMatrix();
      rotate(radians(angle * direction));
      stroke(lerpColor(colorFrom,colorTo,map(i,steps,startSize,0,1)));
      rect(0,0,i,i);
      direction = direction * -1;
      speed = speed * 1.2;
      angle += speed;
    popMatrix();
  }
  speed = initialSpeed;
}

void mousePressed(){
  colorFrom = color(random(360),random(50,100),random(50,100),150);
  colorTo = color(random(360),random(50,100),random(50,100),150);
}