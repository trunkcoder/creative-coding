float angle = 0;
float speed = 0.05;

void setup(){
  size(800,800,P2D);
  strokeWeight(4);
  stroke(255);
}

void draw(){
  background(0);
  translate(400,400);
  
  pushMatrix();
  //stroke(255,0,0);
    rotate(radians(5*angle));
    motif1();
  popMatrix();
  pushMatrix();
    //stroke(0,0,255);
    rotate(radians(-5*angle));
    motif2();
  popMatrix();
  pushMatrix();
    //stroke(0,255,255);
    rotate(radians(10*angle));
    motif3();
  popMatrix();
  angle += speed;
}

void motif1(){
  pushMatrix();
    for(int i = 0; i < 360; i+=5){
      line(0,350,40,250);
      rotate(radians(5));
    }
  popMatrix();
}
void motif2(){
  pushMatrix();
    for(int i = 0; i < 360; i+=5){
      line(0,300,40,150);
      rotate(radians(5));
    }
  popMatrix();
}
void motif3(){
  pushMatrix();
    for(int i = 0; i < 360; i+=5){
      line(0,200,40,100);
      rotate(radians(5));
    }
  popMatrix();
}