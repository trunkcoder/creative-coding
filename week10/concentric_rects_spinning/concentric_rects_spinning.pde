float angle = 0;
float speed = 1;

void setup(){
  size(200,200);
  rectMode(CENTER);
  colorMode(HSB,360,100,100);
  noStroke();
}

void draw(){
  background(200);
  translate(100,100);
  pushMatrix();
    fill(0,25,100);
    rotate(radians(angle));
    rect(0,0,130,130);
  popMatrix();
  pushMatrix();
    fill(70,50,100);
    rotate(radians(-angle));
    rect(0,0,80,80);
  popMatrix();
  pushMatrix();
    fill(20,75,100);
    rotate(radians(angle*5));
    rect(0,0,40,40);
  popMatrix();
  angle += speed;
  if(angle > 359){
   angle = 0; 
  }
}