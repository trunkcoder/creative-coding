
float angle = 0;
float speed = 0.1;
int direction = 1;

color color1;
color color2;

void setup(){
  size(400,400);
  rectMode(CENTER);
  colorMode(HSB,360,100,100);
  color1 = color(359,70,80,125);
  color2 = color(181,70,80,125);
  
  strokeWeight(4);
}

void draw(){
  background(200);
  translate(200,200);
  for(int i = 300; i > 20; i-=20){
    pushMatrix();
      rotate(radians(angle * direction));
      stroke(lerpColor(color1,color2,map(i,20,300,0,1)));
      fill(lerpColor(color2,color1,map(i,20,300,0,1)));
      rect(0,0,i,i);
      angle += speed;
      direction = direction * -1;
    popMatrix();
  }
}

void mousePressed(){
  color1 = color(random(360),random(50,100),random(50,100),125);
  color2 = color(random(360),random(50,100),random(50,100),125);
}