int colour = 0;
int count = 0;
int rate = 30;

void setup(){
  size(800,800);
  colorMode(HSB,360,100,100);
  ellipseMode(CORNER);
  noStroke();
}

void draw(){
  background(0,0,0);
  for(int j = 0; j < 20; j++){
    pushMatrix();
      
      for(int i = 0; i < 20; i++){
        fill(colour,100,100);
        ellipse(0,0,40,40);
        translate(40,0);
      }
      colour += 10;
    popMatrix();
    translate(0,40);
  }
  count += 1;
  //if (count == rate) {
  //  count = 0;
  //  saturation += 10;
  //  if(saturation > 100){
  //    saturation = 0;
  //  }
  //}
  
}