float d = 10.0;
float speed = 2.0;
float angle = 0;
float colourFill = 0;
float colourStroke = 180;
void setup(){
  size(200,200);
  colorMode(HSB,360,100,100);
  stroke(colourStroke,100,100);
  strokeWeight(4);
}

void draw(){
  background(0,100,0);
  translate(100,100);
  rotate(angle);
  fill(colourFill,100,100,100);
  stroke(colourStroke,100,100);
  ellipse(100,0,d,d);//top center
  ellipse(0,100,d,d);//left center
  ellipse(-100,0,d,d);//bottom center
  ellipse(0,-100,d,d);//right center
  d += speed;
  if(d > 400){
    speed *= -1;
  }
  if(d < -400){
    speed *= -1;
  }
  
  //Increasing this value creates
  //persistance of vision effects
  angle += 0.01;
  if(angle > 359){
    angle = 0;
  }
}

void mousePressed(){
  colourFill = random(0,360);
  colourStroke = random(0,360);
}