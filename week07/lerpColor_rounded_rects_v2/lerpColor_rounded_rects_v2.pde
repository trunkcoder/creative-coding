void setup(){
  size(800,800);
  colorMode(HSB,360,100,100);
  noStroke();
}

void draw(){
  background(0,0,0);
  translate(400,400);
  rectMode(CENTER);
  color from = color(274,100,100);
  color to = color(348,4,100);
  for(int i = 0; i < 8; i++){
    //blendMode(SUBTRACT);
    fill(lerpColor(from,to,i/8.0),200);
    rect(190,0,300,150,75);
    rotate(radians(360/8));
  }
}