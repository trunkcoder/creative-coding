int spacing = 3;

void setup(){
  size(800,800);
}

void draw(){
  background(0);
  pushMatrix();
    translate(110,110);
    rowOfFlowers();
  popMatrix();
  
  pushMatrix();
    translate(110,320);
    rowOfFlowers();
  popMatrix();
  
  pushMatrix();
    translate(110,530);
    rowOfFlowers();
  popMatrix();
  
  pushMatrix();
    translate(110,740);
    rowOfFlowers();
  popMatrix();
  
}

void rowOfFlowers(){
  for (int i = 0; i < 6; i++){
      flower();
      translate(210,0);
    }
}

void flower(){
  pushMatrix();
    fill(255,0,0);
    noStroke();
    petal();
    rotate(radians(90));
    fill(255,255,0);
    petal();
    rotate(radians(90));
    fill(255,0,255);
    petal();
    rotate(radians(90));
    fill(0,255,0);
    petal();
  popMatrix();
}

void petal(){
  ellipseMode(CORNER);
  ellipse(spacing,spacing, 100,100);
  rect(spacing,spacing,50,50);
}