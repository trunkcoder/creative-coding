int spacing = 3;

void setup(){
  size(800,800);
}

void draw(){
  background(0);
  pushMatrix();
  
    translate(110,110);
    for (int i = 0; i < 5; i++){
      rowOfFlowers();
      translate(0,210);
    }
    
  popMatrix();
}

void rowOfFlowers(){
  pushMatrix();
    for (int i = 0; i < 6; i++){
        flower();
        translate(210,0);
     }
   popMatrix();
}

void flower(){
  pushMatrix();
    fill(255,255,0);
    noStroke();
    for(int i = 0; i < 4; i++){
      petal();
      rotate(radians(90));
    }
  popMatrix();
}

void petal(){
  ellipseMode(CORNER);
  ellipse(spacing,spacing, 100,100);
  rect(spacing,spacing,50,50);
}