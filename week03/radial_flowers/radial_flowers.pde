void setup(){
  size(800,800);
}

void draw(){
  
  translate(80,75);
  flower();
  
  translate(160,0);
  flower();
  
}

void flower(){
  pushMatrix();
    noStroke();
    //Stem
    fill(#935E09);
    rect(-5,0,5,200);
    
    //Flower back drop
    fill(#F8FC00);
    ellipse(0,0,100,100);
    
    //Flower centre
    fill(#ED71A5);
    ellipse(0,0,70,70);
    
    //petals
    fill(#7FD0ED);
    for (int i = 0; i < 6; i++) {
      ellipse(0,-50,20,40);
      rotate(radians(60));
    }
    
    
  popMatrix();
}