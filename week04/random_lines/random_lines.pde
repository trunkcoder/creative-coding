void setup(){
  size(800,800);
  background(0);
  linesVertical();
}

void draw(){
  
}

void linesVertical(){
  strokeWeight(8);
  for(int i = 1; i < 801; i+=8){
    stroke(random(255),random(255),random(255));
    line(i,0,i,random(200,800));
  }
}

void mousePressed(){
  background(0);
  linesVertical();
}