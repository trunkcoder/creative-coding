void setup(){
  size(800,800);
  verticalLines();
}

void draw(){
  
}

void verticalLines(){
  strokeWeight(12);
 for (int i = 1; i < 801; i+=12){
    stroke(random(255),random(255),random(255));
   line(i,0,i,random(200,800)); 
  } 
}

void horizontalLines(){
  strokeWeight(4);
 for (int i = 1; i < 801; i+=4){
    stroke(random(255),random(255),random(255));
   line(0,i,800,i); 
  } 
}

void mousePressed(){
  background(0);
  verticalLines();
  //if(random(1) > 0.5){
  //  verticalLines();
  //} else {
  //  horizontalLines();
  //}
}