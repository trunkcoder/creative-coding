void setup(){
  size(800,800);
  background(100);
  noFill();
  stroke(255);
  motifFillScreen();
  
}

void draw(){
  
}

void motifFillScreen(){
  
  pushMatrix();
  for(int y = 0; y < 8; y++){
    pushMatrix();
    for(int x = 0; x < 8; x++){
      motif();
      translate(100,0);
    }
    popMatrix();
    translate(0,100);
  }
  popMatrix();
}

void motif(){
  strokeWeight(4);
  for(int i = 20; i < 120; i+=20){
    if(random(1) > 0.8){
      rect(0,0,i,i);
    }
  }
}

void mousePressed(){
  background(100);
  motifFillScreen();
}