void setup(){
  size(800,800);
  motif();
}

void draw(){
  
}

void motif(){
  background(100);
  translate(400,400);
  noStroke();
  for (int j = 0; j < 6; j++){
    for(int i = 0; i < 12; i++){
      ellipse(0,-200,random(50,70),random(30,100));
      rotate(radians(30));
    }
    scale(0.6);
  }
}

void mousePressed(){
  motif();
}