void setup(){
  size(800,800);
  background(100);
  noFill();
  noStroke();
  translate(400,400);
  
  
}

void draw(){
  //Even through we are not using draw
  //we need it for mousePressed function to work
}


void motif(){
  strokeWeight(4);
  for(int i = 100; i > 0; i-=20){
    if(random(1) > 0.4){
      //stroke(random(255),random(255),random(255));
      fill(random(255),random(255),random(255));
      rect(0,0,i,i);
    }
  }
}

void mousePressed(){
  translate(400,400);
  background(100);
  motif();
}