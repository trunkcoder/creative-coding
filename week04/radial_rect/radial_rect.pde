void setup(){
  size(800,800);
  motif();
}

void draw(){
  
}

void motif(){
  background(100);
  translate(100,400);
  strokeWeight(3);
  for(int i = 0; i < 12; i++){
    rectMode(CENTER);
    fill(random(255),random(255),random(255));
    stroke(random(255),random(255),random(255));
    rect(0,0,20,50);
    translate(50,0);
  }
}

void mousePressed(){
  motif();
}