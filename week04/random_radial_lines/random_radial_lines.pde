void setup(){
  size(800,800);
  motif();
}


void draw(){
   
}

void motif(){
  translate(400,400);
   background(100);
   strokeWeight(4);
  for(int i = 0; i < 12; i++){
     float lineLength = random(100,200);
     stroke(random(255),random(255),random(255));
     line(0,-40,0,-lineLength);
     rotate(radians(30));
   }
}