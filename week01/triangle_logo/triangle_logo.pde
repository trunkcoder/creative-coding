//A logo made from triangles

void setup(){
  size(200,200);
}

void draw(){
  background(255);
  fill(0);
  noStroke();
  //top half
  triangle(75,0,0,60,30,60);
  triangle(75,0,60,60,90,60);
  triangle(75,0,120,60,150,60);
  
  //bottom half
  triangle(0,65,75,125,30,65);
  triangle(60,65,75,125,90,65);
  triangle(120,65,75,125,150,65);
}