//A logo made from triangles

void setup(){
  size(200,200);
}

void draw(){
  background(255);
  noStroke();
  fill(200);
  rect(40,40,120,120,20,5,5,5);
  fill(100);
  rect(40,40,80,80,20,5,5,5);
  fill(0);
  rect(40,40,40,40,20,5,5,5);
}
