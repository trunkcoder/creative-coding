PGraphics grid;

void setup(){
  size(800,800);
  createGrid();
}

void draw(){
  //creates the grid
  image(grid,0,0);
  
  //example simple shape used with grid
  stroke(255,0,0);
  noFill();
  strokeWeight(4);
  ellipse(400,400,200,200);
}

void createGrid(){
  grid = createGraphics(width,height);
  grid.beginDraw();
  grid.background(0);
  grid.stroke(0,244,255);
  //vertical lines
  for(int i = 25; i < width; i+=25){
    grid.line(i,0,i,height);
  }
  //horizontal lines
  for(int i = 25; i < height; i+=25){
    grid.line(0,i,width,i);
  }
  grid.endDraw();
}