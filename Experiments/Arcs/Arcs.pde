int arcSize = 10;
int arcType = 1;

void setup(){
 size(400,400);
 background(255);
 strokeWeight(2);
 noFill();
}

void draw(){
 //translate(200,200);
 
 if(arcType == 1){
   stroke(255,0,0);
   arc(0,0,arcSize,arcSize,0,HALF_PI);
   arcSize += 15;
   if (arcSize > 1600) {
     arcType = 2;
     arcSize = 10;
   }
 } else {
   translate(400,400);
   stroke(0,0,255);
   arc(0,0,arcSize,arcSize,PI,PI+HALF_PI);
   arcSize += 15;
 }
}