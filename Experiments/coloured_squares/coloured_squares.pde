int size = 50;

boolean record;

void setup(){
  size(1024,768);
  colorMode(HSB,360,100,100);
  
  background(255);
  noStroke();
  //translate(100,100);
  color c1 = color (random(360),100,100);
  motif(c1);
}

void draw(){
  
}

void motif(color c){
  for(int y = 0; y < 4; y++){
    pushMatrix();
      for(int x = 0; x < 4; x++){
        float h = hue(c);
        float s = saturation(c);
        float b = brightness(c);
        if(random(1.0) > 0.5){
          s = random(30,100);
        } else {
          b = random(30,100);
        }
        fill(h,s,b);
        rect(0,0,size,size);
        translate(size,0);
      }
    popMatrix();
    translate(0,size);
  }
}

void mousePressed(){
  background(255);
  //translate(100,100);
  color c1 = color (random(360),100,100);
  motif(c1);
}