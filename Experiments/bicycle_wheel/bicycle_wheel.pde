void setup(){
  size(800,800);
}

void draw(){
  background(200);
  translate(400,400);
  pushMatrix();
    strokeWeight(30);
    stroke(0);
    fill(255);
    ellipse(0,0,600,600);
    rotate(radians(-5));
    strokeWeight(3);
    twinSpoke();
    for(int i = 0; i < 12; i++){
      rotate(radians(30));
    twinSpoke();
    }
  popMatrix();
  rectMode(CENTER);
  noStroke();
  fill(0);
  rect(0,-310,20,15);
  for(int i = 0; i < 30; i++){
    rotate(radians(12));
    rect(0,-310,20,15);
  }
}

void twinSpoke(){
  pushMatrix();
    line(0,0,0,-300);
    rotate(radians(10));
    line(0,0,0,-300);
  popMatrix();
}
