
void setup(){
 size(800,800);
 noFill();
 strokeWeight(10);
}

void draw(){
  background(0);
  stroke(255);
  translate(250,250);
  for(int j = 0; j < 5; j++) {
    for(int i = 0; i < 5; i++) {
      ellipse(i * 70, j * 70, 50,50);  
    }
  }
  
  
  translate(2,2);
  rotate(radians(1));
  stroke(255,0,0);
  for(int j = 0; j < 5; j++) {
    for(int i = 0; i < 5; i++) {
      ellipse(i * 70, j * 70, 50,50);  
    }
  }
}
