ArrayList<PVector> points;
PVector p;
void setup(){
  size(800,800);
  background(0);
  stroke(255);
  translate(400,400);
  points = new ArrayList<PVector>();
  pointers();
  
}

void draw(){

}

void pointers(){
  for(int i = 0; i < 360; i+=30){
    float x1 = cos(radians(i)) * 350;
    float y1 = sin(radians(i)) * 350;
    p = new PVector(x1,y1);
    points.add(p);
    //print (p);
  }
  //println(points.size());
  
  stroke(random(255),random(255),random(255));
  pointsDrawn();
  drawLines();
  rotate(radians(12));
  stroke(random(255),random(255),random(255));
  pointsDrawn();
  drawLines();
  rotate(radians(12));
  stroke(random(255),random(255),random(255));
  pointsDrawn();
  drawLines();
}

void pointsDrawn(){
  
  strokeWeight(8);
  for(int i = 0; i < points.size(); i++){
    point(points.get(i).x,points.get(i).y);
  }
}

void drawLines(){
  strokeWeight(1);
  for (int j = 0; j < points.size(); j++){
    //stroke(random(255),random(255),random(255));
    for (int i = 0; i < points.size(); i++) {
      line(points.get(j).x,points.get(j).y,points.get(i).x,points.get(i).y);
    }
  }
}

void mousePressed(){
  translate(400,400);
  stroke(random(255),random(255),random(255));
  background(0);
  points = new ArrayList<PVector>();
  pointers();
}