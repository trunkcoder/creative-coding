float[] angles = {360,60,30,20,15,12,10,8.37};
int angleIndex = 0;
float angleSum = 0;
int circleWidth = 50;
void setup(){
 size(800,800);
 background(0);
 noStroke();
}

void draw(){
  circles();
}

void circles(){
  translate(400,400);
  if (angleIndex < angles.length){
    rotate(radians(angleSum));
    if(random(1) < 0.6){
      fill(0,0,255);
    } else {
      fill(255,0,0);
    }
    
    if(angleSum <= (360-angles[angleIndex])) {
      ellipse(angleIndex * circleWidth, 0 , circleWidth, circleWidth);
      angleSum += angles[angleIndex];
    } else {
      angleSum = 0;
      angleIndex += 1;
    }
   }
}

void mousePressed(){
  background(0);
  angleIndex = 0;
  angleSum = 0;
}