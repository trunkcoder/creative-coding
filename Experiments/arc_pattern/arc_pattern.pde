int rowSpacing = 88;

//the value is 140 for the patten elements to 
//line up seamlessly but other values such as 150, 160
//give really nice effects
int patternSpacing =120;

void setup(){
 size(800,800,P2D); 
}

void draw(){
  background(#786B14);
  translate(50,50);
  noFill();
  stroke(#D4BB17);
  strokeWeight(8);
  
  //The basic pattern
  patternFillScreen();
  
  //The same pattern overlaying the previous but with
  //a smaller stroweight and colour
  stroke(#D6631C);
  strokeWeight(2);
  patternFillScreen();
}

void patternFillScreen(){
  pushMatrix();
    patternRow();
    translate(0,rowSpacing);
    patternRow();
    translate(0,rowSpacing);
    patternRow();
    translate(0,rowSpacing);
    patternRow();
    translate(0,rowSpacing);
    patternRow();
    translate(0,rowSpacing);
    patternRow();
    translate(0,rowSpacing);
    patternRow();
    translate(0,rowSpacing);
    patternRow();
  popMatrix();
}

void patternRow(){
  pushMatrix();
    pattern();
    translate(patternSpacing,0);
    pattern();
    translate(patternSpacing,0);
    pattern();
    translate(patternSpacing,0);
    pattern();
    translate(patternSpacing,0);
    pattern();
    translate(patternSpacing,0);
    pattern();
  popMatrix();
}

void pattern(){
  arc(0,0,40,40,0,PI);
  arc(0,0,60,60,0,PI);
  arc(0,0,80,80,0,PI);
  arc(0,0,100,100,0,PI);
  pushMatrix();
    translate(70,0);
    rotate(radians(180));
    arc(0,0,40,40,0,PI);
    arc(0,0,60,60,0,PI);
    arc(0,0,80,80,0,PI);
    arc(0,0,100,100,0,PI);
  popMatrix();
}
