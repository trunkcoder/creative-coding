color c1;
color c2;
int colorIndex = 0;

void setup(){
  size(800,800);
  colorMode(HSB,360,400,400);
  
}

void draw(){
  background(200);
  translate(400,400);
  noStroke();
  for(int i = 400; i > 10; i-=10){
    fill(colorIndex,400,400-i/2);
    ellipse(0,0,i,i);
  }
  pushMatrix();
    for(int i = 0; i < 40; i++){
      //fill(360,0,400,100);
      noFill();
      stroke(360,0,400,100);
      strokeWeight(2);
      triangle(-10,0,10,0,0,180);
      rotate(radians(360/40));
    }
    
  popMatrix();
}

void mousePressed(){
  colorIndex += 10;
}