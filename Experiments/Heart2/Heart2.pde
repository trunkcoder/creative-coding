void setup(){
 size(800,800); 
}

void draw(){
 translate(400,400);
 rotate(radians(135));
 pushMatrix();
   noStroke();
   rectMode(CENTER);
   fill(100);
   rect(0,0,200,200);
   ellipse(0,100,200,200);
   strokeWeight(8);
  stroke(255);
   rotate(radians(90));
   ellipse(0,100,200,200);
 popMatrix();
 pushMatrix();
   fill(0);
   ellipse(0,100,100,100);
   rotate(radians(90));
   ellipse(0,100,100,100);
 popMatrix();
}