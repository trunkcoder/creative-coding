import netP5.*;
import oscP5.*;


int angle = 6;//The smaller the number more circles
int frame = 0;
int pause = 6;
int circleWidth = 300;
int x = 300;
int y = -150;

void setup(){
  size(800,800);
  noFill();
  strokeWeight(4);
  translate(400,400);
}

void draw(){
  frame +=1;
  if(frame > pause){
    background(0);
    translate(400,400);
    spiral();
    frame = 0;
  } 
}

void spiral(){
  for(int i = 0; i < 360; i+=360/angle){
    pushMatrix();
    stroke(random(255),random(255),random(255));
      rotate(radians(i));
      ellipse(x,y,circleWidth,circleWidth);
     popMatrix();
  }
}


void keyPressed(){
  if (key == CODED) {
    if (keyCode == UP) {
      x -= 5;
    }
    if (keyCode == DOWN) {
      x += 5;
    }
    if (keyCode == LEFT) {
      y -= 5;
    }
    if (keyCode == RIGHT) {
      y += 5;
    }
    
  }
  if (keyCode == 65) {
      circleWidth -= 5;
    }
    if (keyCode == 68) {
      circleWidth += 5;
    }
    if (keyCode == 87) {
      angle -= 2;
      if (angle < 2){
        angle = 2;
      }
    }
    if (keyCode == 83) {
      angle += 2;
    }
    
  println(keyCode);
}
