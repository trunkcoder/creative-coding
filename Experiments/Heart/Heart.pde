


void setup(){
 size(400,400);
 strokeWeight(3);
}

void draw(){
 background(255);
 translate(200,200);
 
 //Left half
 stroke(255,0,0);
 arc(-20,0,40,40,HALF_PI,TWO_PI);
 arc(-20,40,40,40,PI+HALF_PI,TWO_PI);
 
 //Right half
 stroke(0,255,0);
 arc(20,0,40,40,PI,TWO_PI+HALF_PI);
 arc(20,40,40,40,PI,PI+HALF_PI);
}