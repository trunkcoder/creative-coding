void setup(){
  size(400,400);
  background(0);
  stroke(255);
  strokeWeight(3);
  noFill();
}

void draw(){
  background(0);
  translate(200,200);
  scale(0.75);
    for(int i = 0; i < 20; i++){
      pushMatrix();
        rotate(radians(i * 18));
        bezier(0.5,0,0,0,73.9,-37.3,127.3,26.7);
        translate(128,27);
        bezier(0,0.3,53.4,64,143.5,0,143.5,0);
      popMatrix();
  }
}