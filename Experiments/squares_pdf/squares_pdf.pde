import processing.pdf.*;

boolean record;

void setup(){
  size(842,595);
  //colorMode(HSB,360,100,100);
  noStroke();
  record = false;
}

void draw(){
  if (record) {
    beginRecord(PDF,"squares-####.pdf");
  }
  if(record){
    motif();
  }
  if(record){
    endRecord();
    record = false;
  }
}

void motif(){
  noStroke();
  for (int y = 0; y < 600; y+= 40){
    pushMatrix();
      for ( int x = 0; x < 880; x+= 40){
        fill(random(255),random(255),random(255));
        rect(0,0,40,40);
        translate(40,0);
      }
    popMatrix();
    translate(0,40);
  }
}

void mousePressed(){
  background(255);
  motif();
}

void keyPressed(){
  record = true;
}