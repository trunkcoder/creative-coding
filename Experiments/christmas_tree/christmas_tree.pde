


void setup(){
  size(800,800);
}

void draw(){
  
  translate(400,400);
  background(200);
  stroke(0);
  strokeWeight(4);

    tree();
  
}

void tree(){
  //triangle(0,0,-108,188,108,188);
  float m1 = abs((0-188.0)/(0-(-108.0)));
  float m2 = abs((0-188.0)/(0-(108.0)));
  for (int x = 0; x <= 108; x+= 5){
    float y1 = m1 * (x+108)-188;
    float y2 = m2 * (x-108)+188;
    stroke(255,0,0);
    //point(-x,y1);
    //point(x,y2);
    line(-x,y1,x,y2);
  }
}