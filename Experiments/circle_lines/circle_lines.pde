void setup(){
  size(1000,800);
  
  checkedCircleLines();
}

void draw(){
  
}

void checkedCircleLines(){
  background(0);
  translate(500,400);
  strokeWeight(3);
  stroke(0);
  circleLines();
  rotate(radians(45));
  circleLines();
  rotate(radians(90));
  circleLines();
}

void circleLines(){
  stroke(random(255),random(255),random(255));
  float x1,x2,y1,y2;
  for(int i = 4; i < 180; i+=4){
    x1 = cos(radians(i)) * 350;
    y1 = sin(radians(i)) * 350;
    x2 = cos(radians(-i)) * 350;
    y2 = sin(radians(-i)) * 350;
    
    line(x1,y1,x2,y2);
  }
}

void mousePressed(){
  checkedCircleLines();
}