color c1;
color c2;
int colorIndex = 0;

void setup(){
  size(800,800);
  colorMode(HSB,360,400,400);
  c1 = color(#FF0303);
  c2 = color(#03FF4C);
}

void draw(){
  background(200);
  translate(400,400);
  
      strokeWeight(3);
  //noStroke();
  for(int i = 400; i > 10; i-=10){
    stroke(lerpColor(c1,c2,map(i,400,10,1,0)));
    ellipse(0,0,i,i);
  }
  pushMatrix();
    for(int i = 0; i < 40; i++){
      //fill(360,0,400,100);
      noFill();
      strokeWeight(2);
      stroke(360,0,400,200);
      triangle(-10,0,10,0,0,180);
      rotate(radians(360/40));
    }
    
  popMatrix();
}

void mousePressed(){
  colorIndex += 10;
}