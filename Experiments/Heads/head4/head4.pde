void setup(){
  size(800,800);
}

void draw(){
  background(100);
  translate(400,400);
  fill(255);
  
  //Head
  ellipse(0,0,300,300);
  
  fill(0);
  //Eye left
  ellipse (-60,-50,50,50);
  
  //Eye right
  ellipse (60,-50,50,50);
  
  fill(255);
  //Nose
  triangle(0,-20,-25,20,25,20);
  
  //Mouth
  fill(0);
  rectMode(CENTER);
  rect(0,60,80,30);
  
  fill(255);
  //Teeth
  triangle(-40,45,-30,90,-20,45);
  triangle(-20,45,-10,60,0,45);
  triangle(0,45,10,60,20,45);
  triangle(20,45,30,90,40,45);
}
