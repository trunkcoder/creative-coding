void setup(){
  size(800,800);
}

void draw(){
  background(100);
  translate(400,400);
  fill(255);
  //Head
  ellipse(0,0,300,300);
  
  fill(0);
  //Eye left
  triangle(-100,-50, -20,-50,-60,0);
  
  //Eye right
  triangle(100,-50, 20,-50,60,0);
  
  //Mouth
  triangle(0,30,-40,80,40,80);
}
