void setup(){
  size(800,800);
}

void draw(){
  background(255);
  translate(400,400);
  strokeWeight(2);
  //Hair
  fill(0);
  ellipse(0,0,40,40);
  ellipse(40,0,40,40);
  ellipse(80,0,40,40);
  ellipse(120,0,40,40);
  fill(255);
  //Head
  rect(-20,0,160,130);
  
  //Eye left
  ellipse(20,50,42,42);
  fill(0);
  ellipse(20,50,15,15);
  
  //Eye right
  fill(255);
  ellipse(100,50,42,42);
  fill(0);
  ellipse(100,50,15,15);
  fill(255);
  //Nose
  ellipse(60,70,18,18);
  
  //Mouth
  ellipse(60,100,42,20);
}
