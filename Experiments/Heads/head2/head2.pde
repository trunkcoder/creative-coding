void setup(){
  size(800,800);
}

void draw(){
  background(100);
  translate(400,400);
  strokeWeight(2);
  fill(255);
  
  //Head
  rectMode(CORNER);
  rect(0,0,160,130);
  
  //Eye left
  ellipse(40,50,40,40);
  
  //Eye right
  ellipse(120,50,40,40);
  
  //Hair
  fill(255,0,0);
  triangle(160,0,90,0,135,90);
  stroke(0);
  //Mouth
  fill(255);
  rectMode(CENTER);
  rect(80,100, 50,20);
  
  //Nose
  triangle(80,60,70,80,90,80);
}
