import processing.pdf.*;

ArrayList<PVector> side1;
ArrayList<PVector> side2;

void setup(){
  beginRecord(PDF,"tree-####.pdf");
  size(800,800);
  
  translate(400,400);
  background(0);
  
  strokeCap(SQUARE);
  strokeWeight(20);
  
  tree();
  endRecord();
}

void draw(){
  
  
}

void tree(){
  side1 = new ArrayList<PVector>();
  side2 = new ArrayList<PVector>();
  //triangle(0,0,-108,188,108,188);
  float m1 = abs((0-188.0)/(0-(-108.0)));
  float m2 = abs((0-188.0)/(0-(108.0)));
  for (int x = 0; x <= 108; x+= 10){
    float y1 = m1 * (x+108)-188;
    side1.add(new PVector(-x,y1));
    float y2 = m2 * (x-108)+188;
    side2.add(new PVector(x,y2));
  }
  
  boolean left = true;
  println(side1.size());
  for(int i = 0; i < side1.size()-1; i++){
    if(left){
      blendMode(ADD);
      stroke(250,0,0);
      line(side1.get(i).x,side1.get(i).y,side2.get(i+1).x,side2.get(i+1).y);
      left = false;
    } else {
      //blendMode(BLEND);
      stroke(0,255,0);
      line(side2.get(i).x,side2.get(i).y,side1.get(i+1).x,side1.get(i+1).y);
      left = true;
    }
  }
}