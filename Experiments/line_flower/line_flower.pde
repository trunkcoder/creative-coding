//I wouldn't have been able to do this code 
//without the following link
//https://processing.org/tutorials/trig/


IntList endOfLines;
int lineAngle = 2; //The higher the number the smaller the number of lines
int lineLength = 200; //The basic line length
int lineRange = 80; //How much the lineLength can vary from

void setup(){
  size(800,800);
  noFill();
  stroke(255);
  strokeWeight(2);
  background(0);
  translate(400,400);
  endOfLines = new IntList();
  flower();
}

void draw(){
  
  
}

void flower(){
  //draw radiated lines from centre
  for (int i = 0; i < 359; i+=lineAngle){
    int offset =floor(random(1,lineRange));
    pushMatrix();
      rotate(radians(i));
      int endOfLine = lineLength + offset;
      endOfLines.append(endOfLine);
      line(0,0,endOfLine,0);
    popMatrix();
  }
  
  //draw line between end of lines
  for (int i = 0; i < endOfLines.size()-1; i++){
    float x = cos(radians(i * lineAngle)) * endOfLines.get(i);
    float y = sin(radians(i * lineAngle)) * endOfLines.get(i);
    float x2 = cos(radians((i+1) * lineAngle)) * endOfLines.get(i+1);
    float y2 = sin(radians((i+1) * lineAngle)) * endOfLines.get(i+1);
    line(x,y,x2,y2);
  }
  //Draw last line to first
  float x = cos(radians((endOfLines.size()-1) * lineAngle)) * endOfLines.get((endOfLines.size()-1));
  float y = sin(radians((endOfLines.size()-1) * lineAngle)) * endOfLines.get((endOfLines.size()-1));
  float x2 = cos(radians(0 * lineAngle)) * endOfLines.get(0);
  float y2 = sin(radians(0 * lineAngle)) * endOfLines.get(0);
  line(x,y,x2,y2);
}

void mousePressed(){
  background(0);
  translate(400,400);
  endOfLines = new IntList();
  flower();
}
