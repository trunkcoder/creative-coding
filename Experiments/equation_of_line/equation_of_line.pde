int x1 = 0;
int y1 = 0;
int x2 = 200;
int y2 =200;
float m = abs((y1 - y2)/(x1-x2));


void setup(){
  size(800,800);
}


void draw(){
  translate(400,400);
  strokeWeight(6);
  stroke(0);
  line(x1,y1,x2,y2);
  for(int x = 0; x <= x2; x+=10){
    float y = m*(x-x2) + 200;
    strokeWeight(6);
    stroke(255,0,0);
    point(x,y);
  }
}