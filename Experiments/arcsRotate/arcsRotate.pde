
int radius = 200;

void setup(){
  size(800,800);
}

void draw(){
  background(200);
  strokeWeight(3);
  translate(400,400);
  noFill();
 for (int i = 400; i > 20; i-= 20){
   arc(0,0,i,i,0,radians(300));
   rotate(radians(i));
 }
  
}
