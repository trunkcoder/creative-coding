int circleDiameter = 12;
int minCircleDiameter = 3;

void setup(){
  size(800,800);
  background(100);
  translate(circleDiameter/2,circleDiameter/2);
  strokeWeight(2);
  noFill();
  motifFillScreen();
}

void draw(){
  
}

void motifFillScreen(){
  for(int j = 0; j < height/circleDiameter; j++){
    pushMatrix();
    for(int i = 0; i < width/circleDiameter; i++){
      pushMatrix();
      translate(random(-minCircleDiameter/2,minCircleDiameter/2),random(-minCircleDiameter/2,minCircleDiameter/2));
      motif();
      popMatrix();
      translate(circleDiameter,0);
    }
    popMatrix();
    translate(0,circleDiameter);
    translate(random(-minCircleDiameter/2,minCircleDiameter/2),random(-minCircleDiameter/2,minCircleDiameter/2));
  }
}

void motif(){
  for(int i = circleDiameter; i > minCircleDiameter;i-= minCircleDiameter){
    if(random(1) > 0.5){
      stroke(random(255),random(255),random(255));
      pushMatrix();
      translate(random(-minCircleDiameter/2,minCircleDiameter/2),random(-minCircleDiameter/2,minCircleDiameter/2));
      ellipse(0,0,i,i);
      popMatrix();
    }
  }
}

void mousePressed(){
  background(100);
  translate(circleDiameter/2,circleDiameter/2);
  noFill();
  motifFillScreen();
}