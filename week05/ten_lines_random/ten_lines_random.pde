void setup(){
  size(800,800);
  background(0);
  strokeWeight(5);
  fillScreen();
}

void draw(){
  
}

void motif(){
  stroke(random(255),random(255),random(255));
  for(int i = 1; i < 80; i+= 8){
    line(0,i,30+random(20,50),i);
  }
}

void fillScreen(){
  for(int y = 0; y < 10; y++){
    pushMatrix();
    for(int x = 0; x < 10; x++){
      motif();
      translate(100,0);
    }
    popMatrix();
    translate(0,100);
  }
}

void mousePressed(){
  background(0);
  fillScreen();
}