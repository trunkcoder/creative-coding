void setup(){
  size(800,800);
  background(0);
  translate(40,40);
  strokeWeight(2);
  noFill();
  fillScreen();
}

void draw(){
  //Even through we are not using the draw
  //function it is still needed for the
  //mousePressed function to work
}

void motif(){
  pushMatrix();
  translate(random(-10,10),random(-10,10));
  for(int i = 72; i > 10; i-=10){
    if(random(1)>0.5){
      stroke(random(255),random(255),random(255));
      //pushMatrix();
      //translate(random(-5,5),random(-5,5));
      ellipse(0,0,i,i);
      //popMatrix();
    }
  }
  popMatrix();
}

void fillScreen(){
  for (int y = 0; y < 10; y++){
    pushMatrix();
      for(int x = 0; x < 10; x++) {
        motif();
        translate(0,80);
      }
    popMatrix();
    translate(80,0);
  }
}

void mousePressed(){
  translate(40,40);
  background(0);
  fillScreen();
}