void setup(){
  size(800,800);
  background(255);
  noFill();
  stroke(0);
  strokeWeight(4);
  translate(100,100);
  fillScreen();
}

void draw(){
  
  
  
}

void fillScreen(){
  for(int y = 0; y < 4; y++){
  pushMatrix();
    for(int x = 0; x < 4; x++){
      //stroke(random(255),random(255),random(255));
      stroke(0);
      motif();
      translate(200,0);
    }
   popMatrix();
   translate(0,200);
  }
}

void motif(){
  pushMatrix();
    rectMode(CENTER);
    for(int i = 200; i > 20; i-=20){
        rotate(radians(random(0,360)));
      if(random(1)>0.5){
        rect(0,0,i,i);
      }
    }
  popMatrix();
}

void mousePressed(){
  background(255);
  
  translate(100,100);
  fillScreen();
}