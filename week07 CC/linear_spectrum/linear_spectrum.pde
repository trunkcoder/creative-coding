void setup(){
  size(400,400);
}

void draw(){
  colorMode(HSB,400,100,100);
  for(int i = 0; i < 400; i++){
    stroke(i,100,100);
    line(0,0,0,400);
    translate(1,0);
  }
}