void setup(){
  size(400,400);
  strokeWeight(2);
  noStroke();
}

void draw(){
  colorMode(HSB,360,360,360);
  translate(200,200);
  for(int i = 0; i < 360; i++){
    pushMatrix();
    fill(50,360,i);
    rotate(radians(i));
    triangle(0,0,0,200,3,200);
    popMatrix();
  }
}