void setup(){
  size(800,800);
  colorMode(HSB,360,20,100);
  strokeWeight(2);
  stroke(360,0,100);//White in the HSB colour space
  //stroke(360,20,0);//Black in the HSB colour space
}

void draw(){
  for(int j = 0; j < 200; j+=10){
    pushMatrix();
      for (int i = 0; i < 20; i++){
        fill(j,i,100);
        rect(0,0,40,40);
        translate(0,40);
      }
    popMatrix();
    translate(40,0);
  }
  
}