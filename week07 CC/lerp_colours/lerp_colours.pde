void setup(){
  size(400,400);
  noStroke();
}

void draw(){
  colorMode(HSB,360,100,100);
  color from = color(0, 100, 100);
  color to = color(117, 100, 100);
  
  for (float i = 0.0; i < 1; i+=0.05){
    color inter = lerpColor(from,to,i);
    fill(inter);
    rect(0,0,20,400);
    translate(20,0);
  }
}