int angle = 0;
int colorNum = 1;


void setup(){
  size(800,800);
  colorMode(HSB,360,20,20);
  background(360,0,0);
  strokeWeight(3);
  stroke(360,0,0);
  noStroke();
}

void draw(){
  background(360,0,0);
  translate(400,400);
  rotate(radians(angle));
  pushMatrix();
    for(int i = 0; i < 20; i++){
      //blendMode(DIFFERENCE);
      fill(colorNum,20,i);
      ellipse(0,160,160,160);
      rotate(radians(18));
    }
  popMatrix();
  angle++;
}

void mousePressed(){
  colorNum = int(random(360));
}