void setup(){
  size(400,400);
  strokeWeight(2);
  noStroke();
}

void draw(){
  colorMode(HSB,360,100,100);
  translate(200,200);
  for(int i = 0; i < 360; i++){
    pushMatrix();
    fill(i,100,100);
    rotate(radians(i));
    triangle(0,0,0,200,3,200);
    popMatrix();
  }
}