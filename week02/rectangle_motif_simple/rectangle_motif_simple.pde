void setup(){
  size(800,800);
}


void draw(){
  noFill();
  rectMode(CENTER);
  strokeWeight(2);
  
  motifRow();
  
  pushMatrix();
    translate(0,200);
    motifRow();
  popMatrix();
  
  pushMatrix();
    translate(0,400);
    motifRow();
  popMatrix();
  
  pushMatrix();
    translate(0,600);
    motifRow();
  popMatrix();
}

void motifRow(){
  pushMatrix();
    translate(100,100);
    motif();
  popMatrix();
  pushMatrix();
    translate(300,100);
    motif();
  popMatrix();
  pushMatrix();
    translate(500,100);
    motif();
  popMatrix();
  pushMatrix();
    translate(700,100);
    motif();
  popMatrix();
}

void motif(){
  //Beginning of design
    rect(0,0,200,200);
    rect(0,0,200,100);
    rect(0,0,100,200);
    rect(0,0,100,50);
    rect(0,0,50,100);
    //End of the design
}
