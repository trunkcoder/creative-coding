void setup(){
  size(800,800); 
}

void draw(){
  translate(400,400);
  
  //background shape
  fill(0);
  noStroke();
  rectMode(CENTER);
  rect(0,0,100,100);
  
  stroke(255);
  strokeWeight(2);
  rect(0,0,85,85);
  
  noStroke();
  pushMatrix();
    rotate(radians(45));
    rect(0,0,100,100);
    
    stroke(255);
    strokeWeight(2);
    rect(0,0,80,80);
  popMatrix();
  
  rect(0,0,70,70);
  
  pushMatrix();
    rotate(radians(45));
    rect(0,0,60,60);
  popMatrix();
  
  rect(0,0,40,40);
  
  pushMatrix();
    rotate(radians(45));
    rect(0,0,25,25);
  popMatrix();
}
