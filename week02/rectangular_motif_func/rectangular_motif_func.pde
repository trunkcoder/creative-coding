void setup(){
  size(800,800); 
}

void draw(){
  background(200);
  pushMatrix();
    translate(80,80);
    motifRow();
  popMatrix();
  
  pushMatrix();
    translate(80,222);
    motifRow();
  popMatrix();
  
  pushMatrix();
    translate(80,364);
    motifRow();
  popMatrix();
  
  pushMatrix();
    translate(80,506);
    motifRow();
  popMatrix();
  
  pushMatrix();
    translate(80,648);
    motifRow();
  popMatrix();
  
}

void motifRow(){
  pushMatrix();
    motif();
    translate(140,0);
    motif();
    translate(140,0);
    motif();
    translate(140,0);
    motif();
    translate(140,0);
    motif();
  popMatrix();
}

void motif(){
  
  pushMatrix();
    //background shape
    fill(0);
    noStroke();
    rectMode(CENTER);
    rect(0,0,100,100);
    
    stroke(255);
    strokeWeight(2);
    rect(0,0,85,85);
    
    noStroke();
    pushMatrix();
      rotate(radians(45));
      rect(0,0,100,100);
      
      stroke(255);
      strokeWeight(2);
      rect(0,0,80,80);
    popMatrix();
    
    rect(0,0,70,70);
    
    pushMatrix();
      rotate(radians(45));
      rect(0,0,60,60);
    popMatrix();
    
    rect(0,0,40,40);
    
    pushMatrix();
      rotate(radians(45));
      rect(0,0,25,25);
    popMatrix();
  popMatrix();
}
