void setup(){
  size(800,800);
}

void draw(){
  translate(400,400);
  segment();
  
  rotate(radians(50));
  segment();
  
}

void segment(){
  fill(#E05119);
  noStroke();
  arc(0,0,300,300,HALF_PI/2,PI-HALF_PI,PIE);
  noFill();
  stroke(255);
  strokeWeight(4);

  arc(0,0,270,270,HALF_PI/2,PI-HALF_PI);
  
  noFill();
  
  strokeWeight(2);
  arc(0,0,200,200,HALF_PI/2,PI-HALF_PI);
  
  strokeWeight(1);
  arc(0,0,100,100,HALF_PI/2,PI-HALF_PI);
  
  fill(255);
  noStroke();
  arc(0,0,30,30,HALF_PI/2,PI-HALF_PI,PIE);
}
