int angle = 0;

void setup(){
  size(800,800);
}

void draw(){
  fill(0);
  rect(0,0,width,height);
  translate(400,400);
  rotate(radians(angle));
  motif();
  angle += 1;
}

void motif(){
  for (int i = 0; i < 12; i++){
    pushMatrix();
      rotate(radians(30 * i));
      fill(255);
      ellipse(0,200,100,100);
     popMatrix();
  }
}