int angle = 0;

void setup(){
  size(800,800);
  colorMode(HSB,360,100,100);
}

void draw(){
  background(0,0,0);
  translate(400,400);
  pushMatrix();
    rotate(radians(angle));
    fill(0,100,100);
    motif();
    fill(0,60,100);
    motif3();
    fill(0,20,100);
    motif5();
   popMatrix();
  pushMatrix();
    rotate(radians(-angle));
    fill(100,100,100);
    motif2();
    fill(100,60,100);
    motif4();
    fill(100,30,100);
    motif6();
   popMatrix();

  angle += 1;
  if(angle == 360){
    angle = 0;
  }
}

void motif(){
  for (int i = 0; i < 12; i++){
    pushMatrix();
      rotate(radians(30 * i));
      ellipse(0,200,100,100);
     popMatrix();
  }
}

void motif2(){
  for (int i = 0; i < 12; i++){
    pushMatrix();
      rotate(radians(30 * i));
      ellipse(0,120,50,50);
     popMatrix();
  }
}

void motif3(){
  for (int i = 0; i < 12; i++){
    pushMatrix();
      rotate(radians(30 * i));
      ellipse(0,80,25,25);
     popMatrix();
  }
}

void motif4(){
  for (int i = 0; i < 12; i++){
    pushMatrix();
      rotate(radians(30 * i));
      ellipse(0,50,15,15);
     popMatrix();
  }
}

void motif5(){
  for (int i = 0; i < 12; i++){
    pushMatrix();
      rotate(radians(30 * i));
      ellipse(0,25,9,9);
     popMatrix();
  }
}

void motif6(){
  for (int i = 0; i < 12; i++){
    pushMatrix();
      rotate(radians(30 * i));
      ellipse(0,15,5,5);
     popMatrix();
  }
}