int angle = 0;
int speed = 3;

void setup(){
  size(200,200);
  rectMode(CENTER);
}

void draw(){
  background(0);
  translate(100,100);
  rotate(radians(angle));
  rect(0,0,130,130);
  angle += speed;
  if(angle > 359){
    angle = 0;
  }
}