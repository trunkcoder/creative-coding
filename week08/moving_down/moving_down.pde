int posY = -90;
int speed = 3;

void setup(){
  size(200,200);
}

void draw(){
  background(0);
  translate(100,posY);
  ellipse(0,0,180,180);
  posY += speed;
  if(posY > height+90){
    posY = 0;
  }
}