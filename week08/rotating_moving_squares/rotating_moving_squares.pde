int posY1 = 0;
int posY2 = 0;
int speed1 = 1;
int speed2 = 2;

void setup() {
  size(800, 800);
  noFill();
  strokeWeight(5);
}                        

void draw() {
  noStroke();
  fill(255, 50);
  rectMode(CORNER);
  rect(0, 0, width, height);
  posY1+= speed1;
  if (posY1 > height) {
    posY1 = 0;
  }
  motif1();
  posY2+= speed2;
  if (posY2 > height) {
    posY2 = 0;
  }
  motif2();
}

void motif1() {
  stroke(255, 0, 0);
  rectMode(CENTER);
  pushMatrix();
  translate(100, posY1);
  rotate(radians(posY1*3));
  rect(0, 0, 100, 100);
  popMatrix();
}

void motif2() {
  stroke(0, 255, 0);
  rectMode(CENTER);
  pushMatrix();
  translate(300, posY2);
  rotate(radians(-posY2*3));
  rect(0, 0, 100, 100);
  popMatrix();
}