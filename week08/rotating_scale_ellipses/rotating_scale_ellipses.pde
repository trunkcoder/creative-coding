int angle = 0;
float size = 0.01;
void setup(){
  size(800,800);
}

void draw(){
  fill(0);
  rect(0,0,width,height);
  translate(400,400);
  rotate(radians(angle));
  scale(size);
  motif();
  angle += 1;
  size += 0.001;
}

void motif(){
  for (int i = 0; i < 12; i++){
    pushMatrix();
      rotate(radians(30 * i));
      fill(255);
      ellipse(0,200,100,100);
     popMatrix();
  }
}