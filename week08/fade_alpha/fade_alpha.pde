float opacity = 0;
int direction = 1;


void setup(){
  size(800,800);
  colorMode(HSB,360,100,100);
}

void draw(){
  background(0,0,0);
  opacity+= direction;
  if(opacity > 360){
    direction = -1;
  } else if(opacity < 0){
    direction = 1;
  }
  fill(opacity,100,100);
  translate(400,400);
  ellipse(0,0,500,500);

}