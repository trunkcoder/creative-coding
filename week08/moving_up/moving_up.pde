int posY;
int speed = 3;


void setup(){
  size(200,200);
  posY = height + 90;
}

void draw(){
  background(0);
  translate(100,posY);
  ellipse(0,0,180,180);
  posY -= speed;
  if(posY < -90){
    posY = height + 90;
  }
}