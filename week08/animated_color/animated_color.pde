int colorNum = 0;
int speed = 3;

void setup(){
  size(200,200);
  colorMode(HSB,360,100,100);
}

void draw(){
  background(0);
  translate(100,100);
  fill(colorNum,100,100);
  ellipse(0,0,180,180);
  colorNum += speed;
  if(colorNum > 360){
    colorNum = 0;
  }
}