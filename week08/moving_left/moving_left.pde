int posX;
int speed = 3;


void setup(){
  size(200,200);
  posX = width + 90;
}

void draw(){
  background(0);
  translate(posX,100);
  ellipse(0,0,180,180);
  posX -= speed;
  if(posX < -90){
    posX = width + 90;
  }
}